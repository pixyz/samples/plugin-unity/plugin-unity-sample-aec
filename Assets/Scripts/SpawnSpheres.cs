﻿using UnityEngine;

public class SpawnSpheres : MonoBehaviour {

    public int frameSkip = 3;
    private int skipedFrames;
    public GameObject prefab;
    public Vector3 size = new Vector3(10, 10, 10);

    void FixedUpdate () {
        if (frameSkip > skipedFrames) {
            skipedFrames++;
            return;
        } else {
            skipedFrames = 0;
        }
        GameObject sphere = GameObject.Instantiate(prefab);
        sphere.transform.SetParent(transform);
        sphere.transform.localPosition = new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));
    }
}
